export class Movie {
	poster_path: string;
	adult: boolean;
	overview: string;
	release_date: Date;
	genre_ids: number[];
	id: number;
	original_title: string;
	original_language: string;
	title: string;
	backdrop_path: string;
	popularity: number;
	vote_count: number;
	video: boolean;
	vote_average: number;
    public readonly base_image_url:string='https://image.tmdb.org/t/p/original/';
}


export interface Actor {
	profile_path: string;
	adult: boolean;
	gender:number;
	known_for_department:"string"
	id: number;
	known_for: Movie[];
	name: string;
	popularity: number;
}
