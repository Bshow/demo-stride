import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MoviesectionComponent } from './components/moviesection/moviesection.component';
import { HttpClientModule } from '@angular/common/http';
import { MovieComponent } from './components/movie/movie.component';
import { GenreComponent } from './components/genre/genre.component';
import { GenreSpecificSectionComponent } from './components/genre-specific-section/genre-specific-section.component';
import {IvyCarouselModule} from 'angular-responsive-carousel';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ActorComponent } from './components/actor/actor.component';
import { CompanyComponent } from './components/company/company.component';
import { TopmovieComponent } from './components/topmovie/topmovie.component';
import { MatIconModule } from '@angular/material/icon';
import { SectionTitleComponent } from './components/section-title/section-title.component';
import { RoutingModule } from './routing/routing.module';
import { HeaderComponent } from './pages/header/header.component';
import { MaterialModule } from './material/material.module';
import { FooterComponent } from './pages/footer/footer.component';
import { ComingsoonComponent } from './components/comingsoon/comingsoon.component';

@NgModule({
  declarations: [
    AppComponent,
    MoviesectionComponent,
    MovieComponent,
    GenreComponent,
    GenreSpecificSectionComponent,
    DashboardComponent,
    ActorComponent,
    CompanyComponent,
    TopmovieComponent,
    SectionTitleComponent,
    HeaderComponent,
    FooterComponent,
    ComingsoonComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    IvyCarouselModule,
    MatIconModule,
    RoutingModule, 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
