import { Component, OnInit } from "@angular/core";
import { Company } from "src/app/models/company";
import { Genre } from "src/app/models/genre";

import { Actor, Movie } from "src/app/models/movie";
import { MoviedbService } from "src/app/services/moviedb.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit {
  movies: Movie[][] = [];
  actors: Actor[] = [];
  companies: Company[] = [];
  genres: Genre[] = [];
  constructor(private movieService: MoviedbService) {}

  ngOnInit(): void {
    this.movieService.getPopularMovies(1).then((res) => {
      this.movies[0] = res;
      console.log(res);
    });
    this.movieService.getPopularMovies(2).then((res) => {
      this.movies[1] = res;
      console.log(res);
    });
    this.movieService.getActors(1).then((res) => {
      this.actors = res;
      console.log(res);
    });

    this.movieService.getCompanies("IT").then((res) => {
      this.companies = res;
    });
    this.movieService.getGenres().then((res) => {
      this.genres = res;
      console.log(res);
    });
  }
}
