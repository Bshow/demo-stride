import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Actor, Movie } from '../models/movie';
import { HttpClient } from '@angular/common/http';
import { Genre } from '../models/genre';
import { Company } from '../models/company';

@Injectable({
  providedIn: 'root'
})
export class MoviedbService {

  private apiKey="6ab6d103cf2ba85d668cee4e2de24983";
  constructor(private http: HttpClient) { }

  public getPopularMovies(page:number):Promise<Movie[]>{
    
    return this.http.get<any>(environment.apiUrl +`movie/popular?api_key=${this.apiKey}&page=${page}`).toPromise().then((res)=>{ return this.extractData<Movie>(res)}).catch(this.handleError);
  }

  public getTopMovies(page:number):Promise<Movie[]>{
    
    return this.http.get<any>(environment.apiUrl +`movie/top_rated?api_key=${this.apiKey}&page=${page}`).toPromise().then((res)=>{ return this.extractData<Movie>(res)}).catch(this.handleError);
  }

  public getGenres():Promise<Genre[]>{
    return this.http.get<any>(environment.apiUrl+`genre/movie/list?api_key=${this.apiKey}&language=en-US`).toPromise().then((res)=>{return this.extractGenres(res)}).catch(this.handleError);
  }

  public getActors(page:number):Promise<Actor[]>{
    return this.http.get<any>(environment.apiUrl+`person/popular?api_key=${this.apiKey}&language=en-US&page=${page}`).toPromise().then((res)=>{return this.extractData<Actor>(res)}).catch(this.handleError);
  }
  public getCompanies(region:string):Promise<Company[]>{
    return this.http.get<any>(environment.apiUrl+`watch/providers/tv?api_key=${this.apiKey}&language=en-US&watch_region=${region}`).toPromise().then((res)=>{return this.extractData<Company>(res)}).catch(this.handleError);
  }
  
  private extractGenres(res){
    if(res){
      let genres: Genre[]=[];
      const g= res.genres;
      g.forEach(element => {
        genres.push(element as Genre);
      });
      return genres;
    }
    return [];

  }

  private extractData<T>(res){
    if(res){
      let arr: T[]=[];
      const g= res.results;
      g.forEach(element => {
        arr.push(element as T);
      });
      return arr;
    }
    return [];

  }

  private handleError(error: any) {
    
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Promise.reject(errMsg);
  }
}
