import { Component, Input, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie';

@Component({
  selector: 'app-topmovie',
  templateUrl: './topmovie.component.html',
  styleUrls: ['./topmovie.component.scss']
})
export class TopmovieComponent implements OnInit {

  @Input() info: Movie;
  constructor() { }

  ngOnInit(): void {
  }

}
