import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopmovieComponent } from './topmovie.component';

describe('TopmovieComponent', () => {
  let component: TopmovieComponent;
  let fixture: ComponentFixture<TopmovieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopmovieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopmovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
