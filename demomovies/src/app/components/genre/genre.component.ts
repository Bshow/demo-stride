import { Component, Input, OnInit } from '@angular/core';
import { Genre } from 'src/app/models/genre';


@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.scss']
})
export class GenreComponent implements OnInit {
  @Input() info: Genre; 
  @Input() index:number;

  color1:string;
  color2:string;

  colors : Color[] = [
    new Color('#FD093F', '#F383F1'),
    new Color('#0FFFDA', '#3CDB77'),
    new Color('#B936FF', '#57DEDA'),
    new Color('#FD093F', '#FCCB1A'),
    new Color('#FF6472', '#FDA75D'),
    new Color('#13547A', '#80D0C7'),
    new Color('#FFF77B', '#FFBF42'),
    new Color('#1FA2FF', '#1F535C'),
  ] 

  constructor() { }

  ngOnInit(): void {
    console.log(this.index)
    console.log("Init")

    this.retrievePersonalColor();
    }

  ngOnChanges(){
    this.retrievePersonalColor();
    
    console.log(this.index)
    console.log("CHanges")
  }

  retrievePersonalColor(): void{
    let c : Color = this.colors[this.index % this.colors.length]
    this.color1 = c.color_top_left;
    this.color2 = c.color_bottom_right;
  }

  bclick(){
    console.log(this.info)
  }
}


class Color {
  public color_top_left : string;
  public color_bottom_right : string;
  public constructor(ctl : string, cbr: string){
    this.color_top_left = ctl;
    this.color_bottom_right = cbr;
    console.log("ctor")
  }
}