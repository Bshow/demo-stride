import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenreSpecificSectionComponent } from './genre-specific-section.component';

describe('GenreSpecificSectionComponent', () => {
  let component: GenreSpecificSectionComponent;
  let fixture: ComponentFixture<GenreSpecificSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenreSpecificSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenreSpecificSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
