import { INFERRED_TYPE } from '@angular/compiler/src/output/output_ast';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Genre } from 'src/app/models/genre';

@Component({
  selector: 'app-genre-specific-section',
  templateUrl: './genre-specific-section.component.html',
  styleUrls: ['./genre-specific-section.component.scss']
})
export class GenreSpecificSectionComponent implements OnInit, OnChanges {
  @Input() data : Genre[]; 
  @Input() objRef : any;

  hidden:boolean = false;

  constructor() { }

  ngOnInit(): void {
  }
  ngOnChanges(){
  }

  titleClicked(ev:any){
    this.hidden = (ev as boolean)
  }

}
