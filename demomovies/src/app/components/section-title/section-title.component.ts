import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-section-title',
  templateUrl: './section-title.component.html',
  styleUrls: ['./section-title.component.scss']
})
export class SectionTitleComponent implements OnInit {
  @Input() title: string;
  @Input() title_color: string;
  @Output() onClicked : EventEmitter<boolean> = new EventEmitter<boolean>();

  arrowState: boolean = false;


  constructor() { }

  ngOnInit(): void {
  }

  arrowClick(){
    this.arrowState = !this.arrowState
    this.onClicked.emit(this.arrowState);
  }

}
