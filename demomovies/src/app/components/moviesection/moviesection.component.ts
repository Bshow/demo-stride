import { Component, Input, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie';
import { MoviedbService } from 'src/app/services/moviedb.service';

@Component({
  selector: 'app-moviesection',
  templateUrl: './moviesection.component.html',
  styleUrls: ['./moviesection.component.scss']
})
export class MoviesectionComponent implements OnInit {
  @Input() title:string;
  @Input() objRef:any;
  @Input() data:any[];
  @Input() height:number;
  constructor() { }

  ngOnInit(): void {
    
  }
  titleClicked(ev:any){
    console.log("go to full list")
  }

}
