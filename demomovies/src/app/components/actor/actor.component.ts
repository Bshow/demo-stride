import { Component, Input, OnInit } from '@angular/core';
import { Actor } from 'src/app/models/movie';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.scss']
})
export class ActorComponent implements OnInit {

  @Input() info: Actor;
  @Input() size:number;
  constructor() { }

  ngOnInit(): void {
  }
}
